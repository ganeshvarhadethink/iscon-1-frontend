export const environment = {
  production: true,
  environmentName: 'PROD',
  baseUrl: 'http://localhost:8080/v2/',
};
