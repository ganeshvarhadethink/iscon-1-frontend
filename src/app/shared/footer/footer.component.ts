import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { ApiService } from '../service/api.service';

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"]
})
export class FooterComponent implements OnInit {

  filters : any = [];
  textSearch: string;
  appList: any = [];
  constructor(private route: Router, private apiService: ApiService) {}

  ngOnInit(): void {
    this.getfilters();
  }
  goToHome() {
    this.route.navigate(["/"]);
  }

  getfilters() {
    this.apiService.getCollectionCategoryfilters().subscribe((data: any) => {
      data.list.forEach(element => {
        if(element.name == "Collections"){
        this.filters = element.values; 
      }     
      });
    });
  }

  navigateAppList(selectedItem) {
    this.textSearch = "";
    this.apiService.getSelectedItemApps(selectedItem).subscribe((data: any) => {
      console.log("data list", data.list);
      this.appList = data.list;
      this.apiService.$selectedOptionSubject.next({
        list: this.appList,
        selectedName: selectedItem
      });
      this.route.navigate(["/app-list"]);
    });
  }
}
