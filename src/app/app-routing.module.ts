import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppListComponent } from "./modules/main/app-list/app-list.component";
import { HomeComponent } from "./modules/main/home/home.component";
import { AppDetailsComponent } from "./modules/main/app-details/app-details.component";
import { AppDescriptionComponent } from "./modules/main/app-description/app-description.component";
import { NotFoundComponent } from "./modules/not-found/not-found.component";
import { LoginComponent } from './modules/login/login.component';
import { SignupComponent } from './modules/signup/signup.component';
import { UserProfileComponent } from './modules/main/user-profile/user-profile.component';
import { ForgotPasswordComponent } from './modules/forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
      path: "forgot-password",
      component: ForgotPasswordComponent
  },
  {
    path: "signup",
    component: SignupComponent
  },{
    path: "user-profile",
    component:UserProfileComponent,
  },
  
  {
    path: "",
    component: HomeComponent,
    children: [
      //<---- child components declared here
      {
        path: "",
        component: AppDescriptionComponent
      },
      {
        path: "app-list",
        component: AppListComponent
      }
    ]
  },
  {
    path: "app-details/:safeName",
    component: AppDetailsComponent
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: "enabled"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
